The distantparent module lets you make one taxonomy term the child of another term in a different vocabulary. Distant parent relationships are added and edited through the administration interface.

Note that this module is only concerned with the management of distant parents, not the actual use of them by Drupal's taxonomy system. See GreenAsh's cross-vocabulary hierarchies article (at http://www.greenash.net.au/posts/thoughts/cross_vocab_taxonomy_hierarchies) for a tutorial (with code samples, screenshots, and patch files) explaining how to get breadcrumbs that are distant-parent aware.

INSTALLATION

"distantparent.module" should be placed in the modules folder. (Alternatively, the entire "distantparent" folder can be uploaded with the module file in it.)

You must run the database script in "distantparent.mysql" in order for this module to work.

distantparent.module must be activated via 'administer/modules'. 

USAGE

See the documentation in the Drupal interface.